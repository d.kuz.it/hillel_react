import React from 'react';
// import '../App.css';

class PersonTwo extends React.Component {
  constructor(props) {
    super(props);
    this.state = { name: 'Stepan', age: 25 };
  }
  setAttr = () => {
    const text = document.querySelector(`p.second_text`);
    const opacityValue = text.style.opacity;
    if (opacityValue == true) {
      text.setAttribute('style', 'opacity: 0');
    } else {
      text.setAttribute('style', 'opacity: 1');
    }
  };

  render() {
    return (
      <div>
        <p className="second_text" style={{ opacity: 1 }}>
          Name: {this.state.name}, age: {this.state.age}
        </p>
        <button onClick={this.setAttr}>Click on me</button>
      </div>
    );
  }
}

export default PersonTwo;

