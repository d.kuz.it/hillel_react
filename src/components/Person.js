import React from 'react';

class Person extends React.Component {
  constructor(props) {
    super(props);
    this.state = { name: 'Stepan', age: 25 };
  }
  setNameAge = () => {
    this.setState({
      name: 'Mykola',
      age: 30,
    });
  };

  render() {
    return (
      <div>
        <p>
          Name: {this.state.name}, age: {this.state.age}
        </p>
        <button onClick={this.setNameAge}>Click on me</button>
      </div>
    );
  }
}

export default Person;
