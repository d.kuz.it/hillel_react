import React from 'react';

class PersonThree extends React.Component {
  constructor(props) {
    super(props);
    this.state = { name: 'Stepan', age: 25, isOn: true };
  }
  setAttr = () => {
    this.setState((prevState) => ({
      isOn: !prevState.isOn,
    }));

    const text = document.querySelector(`p.three_text`);
    const opacityValue = text.style.opacity;

    if (opacityValue == true) {
      text.setAttribute('style', 'opacity: 0');
    } else {
      text.setAttribute('style', 'opacity: 1');
    }
  };

  render() {
    return (
      <div>
        <p className="three_text" style={{ opacity: 1 }}>
          Name: {this.state.name}, age: {this.state.age}
        </p>
        <button className="btn" onClick={this.setAttr}>
          {this.state.isOn ? 'Скрыть Текст' : 'Показать Текст'}
        </button>
      </div>
    );
  }
}

export default PersonThree;
