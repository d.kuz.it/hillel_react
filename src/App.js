import './App.css';
import Person from './components/Person';
import PersonThree from './components/PersonThree';
import PersonTwo from './components/PersonTwo';

function App() {
  return <div className="App">
    <Person />
    <PersonTwo />
    <PersonThree />
  </div>;
}

export default App;
